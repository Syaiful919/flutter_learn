import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MovieApps extends StatefulWidget {
  @override
  _MovieAppsState createState() => _MovieAppsState();
}

class _MovieAppsState extends State<MovieApps> {

  List<Container> listMovie = new List();

  var movies = [
    {"name": "Movie 1", "img": "m1.jpg"},
    {"name": "Movie 2", "img": "m2.jpg"},
    {"name": "Movie 3", "img": "m3.jpg"},
    {"name": "Movie 4", "img": "m4.jpg"},
    {"name": "Movie 5", "img": "m5.jpg"},
    {"name": "Movie 6", "img": "m6.jpg"},
    {"name": "Movie 7", "img": "m7.jpg"},
    {"name": "Movie 8", "img": "m8.jpg"},
    {"name": "Movie 9", "img": "m9.jpg"},
    {"name": "Movie 10", "img": "m10.jpg"},
  ];

  _makeDataList(){
    for(var i=0; i<movies.length; i++){
      final list = movies[i];
      final String images = list["img"];

      listMovie.add(new Container(
        padding: new EdgeInsets.all(20),
        child: new Card(
          child: new InkWell(
            onTap:(){
              Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => DetailMovieApps(
                  name: list["name"],
                  img: list["img"],
                )
              ));
            },
            child: new Column(
              children: <Widget>[
                new Padding(padding: new EdgeInsets.all(10),),
                new Hero(
                  tag: list["name"], 
                  child: new Image.asset("img/$images",
                    height: 175,
                    width: 125,
                    fit: BoxFit.cover,),),
                new Padding(padding: new EdgeInsets.all(10)),
                new Text(list["name"], style: new TextStyle(
                  fontWeight: FontWeight.bold, 
                  fontSize: 18
                ),)
              ]
            ),
          ),)
        ,));
    }
  }

  @override
  void initState(){
    super.initState();
    _makeDataList();
  }

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    final double itemHeight = (size.height - kToolbarHeight - 24)/2;
    final double itemWidth = size.width/2;

    return Scaffold(
      appBar: AppBar(
        title: Text("Movie Apps"),
        backgroundColor: Colors.red,
      ),
      resizeToAvoidBottomPadding: false,
      body: new Container(
        child: GridView.count(
          childAspectRatio: (itemWidth/itemHeight),
          shrinkWrap: true,
          controller: new ScrollController(keepScrollOffset: false),
          children: listMovie,
          crossAxisCount: 2)
      ),
    );
  }
}

class DetailMovieApps extends StatelessWidget {

  DetailMovieApps({this.name, this.img});
  final String name;
  final String img;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          new Container(
            height: 300,
            child: new Hero(tag: name, child: new Material(
              child: new InkWell(
                child:Image.asset("img/$img",
                  fit: BoxFit.fitHeight,
                )
              ),),)
          ),
          new NameSection(
            name: name
          ),
          new IconSection(),
          new DescSection()
        ],),
    );
  }
}

class NameSection extends StatelessWidget {

  NameSection({this.name});
  final String name;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: new Row(
        children: <Widget>[
          new Expanded(child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                name, 
                style: new TextStyle(fontSize: 20, color: Colors.red)),
              new Text(
                "$name\@gmail.com",
                style: new TextStyle(
                  fontSize: 17,
                  color: Colors.grey
                ),
              )
            ],
          ),),
          new Row(children: <Widget>[
            new Icon(Icons.star, size: 30, color: Colors.yellow,),
            new Text("12", style: new TextStyle(fontSize: 18),)
          ],)
        ]
      ),
    );
  }
}

class IconSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: new Row(children: <Widget>[
        new IconText(
          iconData: Icons.call,
          text: "call"
        ),
         new IconText(
          iconData: Icons.message,
          text: "message"
        ),
         new IconText(
          iconData: Icons.photo,
          text: "photo"
        ),
      ],)
    );
  }
}

class IconText extends StatelessWidget {
  IconText({this.iconData, this.text});
  final IconData iconData;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: new Column(
        children: <Widget>[
          new Icon(iconData, size: 20, color: Colors.red),
          new Text(text, style: new TextStyle(fontSize: 12, color: Colors.red),)
        ]
      ),
    );
  }
}

class DescSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: new Card(
        child: new Padding(padding: const EdgeInsets.all(10),
          child: new Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat sapien massa, vestibulum imperdiet lorem malesuada eu. Sed vulputate diam ut dolor tincidunt, eu convallis urna iaculis. Duis pharetra varius enim et porttitor. Donec id odio finibus purus blandit pharetra. Morbi posuere, nulla et rhoncus ultricies, purus lacus interdum nisl, vitae aliquet sapien sem sit amet ipsum. Sed pellentesque, arcu quis porta suscipit, nibh nibh finibus nibh, non gravida felis ante mattis arcu. Phasellus vitae lorem arcu. Aliquam vel massa non ex mollis sollicitudin nec pellentesque sapien. Sed venenatis neque id purus molestie, a auctor nibh condimentum. Integer id erat posuere, egestas turpis ut, varius leo. Nullam venenatis, nibh sed consectetur porttitor, arcu elit dignissim massa, nec malesuada erat elit sed nulla. Suspendisse eu lacus in nibh gravida pharetra. Integer tempus maximus nisi eu lobortis. Morbi maximus id nulla non mollis. Quisque finibus volutpat sem.",
          style: new TextStyle(fontSize: 14),
          textAlign: TextAlign.justify,
        ),
        )
      )
    );
  }
}


