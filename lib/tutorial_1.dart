import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';


class PageImageAssets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Img Assets"),
        backgroundColor: Colors.indigo,),
      body: Column(
        children: <Widget>[
          Image.asset("img/gun_phoenix.jpg"),
          Text("Gun Phoenix")
        ]
      ),
    );
  }
}

class PageImageNetwork extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Img Network"),
        backgroundColor: Colors.lightGreen,
      ),
      body: Column(
        children: <Widget>[
          Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg"),
          Text("Naruto vol 01"),
          SizedBox(height: 25,),

          Text("Img from URL", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.orange),),
          SizedBox(height: 15,),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg",
                height: 100,
                width: 100,
              ),
              Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg",
                height: 100,
                width: 100,
              ),
              Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg",
                height: 100,
                width: 100,
              ),
              Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg",
                height: 100,
                width: 100,
              ),
              

            ]
          )
        ]
      ),
    );
  }
}

class PageMainAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animation"),
        backgroundColor: Colors.grey,
      ),
      body: GestureDetector(
        child: Hero(
          tag: "imageHero",
          child: Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg"),),
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (_){
            return DetailHeroAnimation();
          }));
        },),
    );
  }
}

class DetailHeroAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Hero(
          tag: "imageHero",
          child: Image.network("https://upload.wikimedia.org/wikipedia/en/9/94/NarutoCoverTankobon1.jpg"),
        ),
        onTap: (){
          Navigator.pop(context);
        },
      ),
    );
  }
}

class PageAlertDialog extends StatefulWidget {
  @override
  _PageAlertDialogState createState() => _PageAlertDialogState();
}

class _PageAlertDialogState extends State<PageAlertDialog> {
  SimpleDialog simpleDialog;

  final GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();

  void showSnackbar(){
    _key.currentState.showSnackBar(new SnackBar(content: new Text("this is snackbar"),));
  }

  void showSimpleDialog(){
    simpleDialog = new SimpleDialog(
      title: new Text("Warning"),
      children: <Widget>[
        new SimpleDialogOption(
          child: Text("Jakarta"),
          onPressed:(){
            print("Jakarta");
          }
        ),
        new SimpleDialogOption(
          child: new Text("Padang"),
          onPressed:(){
            print("Padang");
          }
        ),
        new SimpleDialogOption(
          child: new Text("Close"),
          onPressed:(){
            Navigator.pop(context);
          }
        )
      ],
    );
    showDialog(context: context, child: simpleDialog);
  }

  void showAlertDialog(){
    showDialog(
      context: context,
      child: new AlertDialog(
        title: Text("Warning"),
        content: Text("Anda yakin ingin keluar ?"),
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.close), 
            onPressed: (){
              Navigator.pop(context);
            }),
          new IconButton(
            icon: Icon(Icons.check, color: Colors.green,), 
            onPressed: (){
              Navigator.pop(context);
            })
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Notif Widget"),
        backgroundColor: Colors.green,
      ),
      key: _key,
      body: new Center(
        child: new Column(
          children: <Widget>[
            new RaisedButton(
              onPressed: (){showSimpleDialog();},
              child: new Text("Show Alert Dialog"),
            ),
            new MaterialButton(
              onPressed: (){showSnackbar();},
              child: Text("show snackbar"),
              color: Colors.orange,
              textColor: Colors.white,
            ),
            new MaterialButton(
              onPressed: (){
                Fluttertoast.showToast(
                  msg: "ini toast",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  backgroundColor: Colors.green,
                  textColor: Colors.white);
              },
              child: Text("show toast"),
              color: Colors.purple,
              textColor: Colors.white,
            ),
            new RaisedButton(
              child: Text("show alert"),
              onPressed: (){
                showAlertDialog();
              })
          ]
        ),
        
      ),
    );
  }
}

class PageSliderWidget extends StatefulWidget {
  @override
  _PageSliderWidgetState createState() => _PageSliderWidgetState();
}

class _PageSliderWidgetState extends State<PageSliderWidget> {
  double drag = 1.0;

  void methodDragVolume(val){
    setState(() {
      drag = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Slider"),
        backgroundColor: Colors.orange,
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Slider(
              value: drag,
              min: 1.0,
              max: 2.0, 
              onChanged: (double val){
                methodDragVolume(val);
              }
            ),
            new Text("value : $drag")
          ],
        )
      ),
    );
  }
}

class PageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Button"),
        backgroundColor: Colors.greenAccent,),
      body: new Column(
        children: <Widget>[
          new RaisedButton(
            onPressed: (){},
            child: Text("Button Raised"),
          ),
          new FlatButton(
            onPressed: (){}, 
            child: Text("Button Flat")),
          new MaterialButton(
            onPressed: (){}, 
            child: Text("Button Material"),
            color: Colors.green,
            textColor: Colors.white,)
        ]
      ),
    );
  }
}

class PageInputWidget extends StatefulWidget {
  @override
  _PageInputWidgetState createState() => _PageInputWidgetState();
}

class _PageInputWidgetState extends State<PageInputWidget> {
  String txt = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: new Text("Page Input Widget"),
      ),

      body: new Column(
        children: <Widget>[
          new TextField(
            onChanged: (String text){
              setState((){
                txt = text;
              });
            },
            decoration: new InputDecoration(
              hintText: "Input Username",
              labelText: "username"
            ), 
          ),
          SizedBox(height:10),
          new Text(txt)
        ]
      ),
    );
  }
}

class PageTabbar extends StatefulWidget {
  @override
  _PageTabbarState createState() => _PageTabbarState();
}

class _PageTabbarState extends State<PageTabbar> with SingleTickerProviderStateMixin{
  TabController tabController;

  @override
  void initState() {

    super.initState();
    tabController = new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tab Bar Apps"),
        backgroundColor: Colors.green,
        bottom: new TabBar(
          controller: tabController,
          tabs: <Widget>[
            new Tab(icon: new Icon(Icons.home),),
            new Tab(icon: new Icon(Icons.dashboard),),
            new Tab(icon: new Icon(Icons.favorite),),
        ]),
      ),

      body: new TabBarView(
        controller: tabController,
        children: <Widget>[
          new Center(child: new Text("Home"),),
          new Center(child: new Text("Dashboard"),),
          new Center(child: new Text("Data Usage"),),
        ]
        ),
    );
  }
}

