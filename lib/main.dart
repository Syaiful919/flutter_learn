import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: ExDatePicker(), debugShowCheckedModeBanner: false,
    );
  }
}

class ExDatePicker extends StatefulWidget {
  @override
  _ExDatePickerState createState() => _ExDatePickerState();
}

class _ExDatePickerState extends State<ExDatePicker> {
  String _val = "";

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
      context: context, 
      initialDate: new DateTime.now(), 
      firstDate: new DateTime(2016), 
      lastDate: new DateTime(2021)
    );
    if(picked != null){
      setState(() {
        _val = picked.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Date Picker Example")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32),
        child: new Center(child: new Column(
          children: <Widget>[
            new Text(_val),
            new RaisedButton(onPressed: _selectDate, child: new Text("Pick a date"))
          ]
        ),),
      ),
    );
  }
}

class ExSwitch extends StatefulWidget {
  @override
  _ExSwitchState createState() => _ExSwitchState();
}

class _ExSwitchState extends State<ExSwitch> {
  bool _val1 = false;
  bool _val2 = false;

  void _onChanged1(bool val) => setState(()=> _val1 = val);
  void _onChanged2(bool val) => setState(()=> _val2 = val);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Switch Example")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32),
        child: new Center(
          child: new Column(children: <Widget>[
            new Switch(value: _val1, onChanged: _onChanged1,),
            new SwitchListTile(
              value: _val2, 
              onChanged: _onChanged2,
              title: new Text("Switch",
                style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.red
                ),
              ),
              )
          ],)
        ),
      ),
    );
  }
}

class ExRadioButton extends StatefulWidget {
  @override
  _ExRadioButtonState createState() => _ExRadioButtonState();
}

class _ExRadioButtonState extends State<ExRadioButton> {
  int _val1 = 2;
  int _val2 = 0;

  void _setVal1(int val) => setState(() => _val1 = val);
  void _setVal2(int val) => setState(() => _val2 = val);

  Widget makeRadios(){
    List<Widget> list = new List<Widget>();

    for(int i = 0; i < 3; i++){
      list.add(new Radio(value: i, groupValue: _val1, onChanged: _setVal1));
    }

    Column column = new Column(children: list);
    return column;
  }

  Widget makeRadioTiles(){
    List<Widget> list = new List<Widget>();

    for(int i = 0; i < 3; i++){
      list.add(new RadioListTile(
        value: i, 
        groupValue: _val2, 
        onChanged: _setVal2,
        activeColor: Colors.green,
        controlAffinity: ListTileControlAffinity.trailing,
        title: new Text("Item: $i"),
        subtitle: new Text("subtitle"),
        ));
    }

    Column column = new Column(children: list);
    return column;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Radio Button Example")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32),
        child: new Center(
          child: new Column(
            children: <Widget>[
              makeRadios(),
              makeRadioTiles()
            ]
          )
        ),
      ),
    );
  }
}

class ExCheckbox extends StatefulWidget {
  @override
  _ExCheckboxState createState() => _ExCheckboxState();
}

class _ExCheckboxState extends State<ExCheckbox> {

  bool _val1 = false;
  bool _val2 = false;

  void _val1Changed(bool val) => setState(() => _val1 = val);
  void _val2Changed(bool val) => setState(() => _val2 = val);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Checkbox Example")
      ),
      body: new Container(
        color: Colors.greenAccent,
        padding: new EdgeInsets.all(32),
        child: new Center(
          child: new Column(children: <Widget>[
            new Checkbox(value: _val1, onChanged: _val1Changed,),
            new CheckboxListTile(
              value: _val2, 
              onChanged: (val){
                _val2Changed(val);
              },
              title: new Text("CheckboxListTile"),
              controlAffinity: ListTileControlAffinity.leading,
              subtitle: new Text("Subtitle"),
              secondary: new Icon(Icons.edit),
              activeColor: Colors.red,
              checkColor: Colors.white,
              )
          ],)
        ),
      ),
    );
  }
}

class ExTextField extends StatefulWidget {
  @override
  _ExTextFieldState createState() => _ExTextFieldState();
}

class _ExTextFieldState extends State<ExTextField> {

  String _val = "";

  void _onChange(String val){
    setState(() {
      _val = "Change: $val";
    });
  }

  void _onSubmit(String val){
    setState(() {
      _val = "Submit: $val";
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Text Field Example")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new TextField(
                decoration: new InputDecoration(
                  labelText: "Label",
                  hintText: "Hint",
                  icon: new Icon(Icons.edit)
                ),
                autocorrect: true,
                autofocus: false,
                keyboardType: TextInputType.text,
                onChanged: _onChange,
                onSubmitted: _onSubmit,
              ),
              new Text(_val)
            ]
          )
        )
      ),
    );
  }
}